# exWx/setup.py
#
## python setup.py py2exe
#
from distutils.core import setup
import os, sys
import ConfigParser
import matplotlib
#noinspection PyUnresolvedReferences
import py2exe
#
sys.argv.append('py2exe')

# Read configuration data from install.ini:
config = ConfigParser.ConfigParser()
config.readfp(open('install.ini'))
#
PyName = config.get('py2exe', 'pyname')
AppVersion = config.get('Common', 'version')
AppName = config.get('Common', 'name')
ImgDir = config.get('Common', 'srcimgdir')
Icon = config.get('Common', 'big_icon')
Sicon = config.get('Common', 'small_icon')
Icon = os.path.join(ImgDir, Icon)
Sicon = os.path.join(ImgDir, Sicon)
SrcDocDir = config.get('Common', 'srcdocdir')
Copyright = config.get('Common', 'copyright')


def get_docs(folder='doc'):
    """
    Get the list of documentation files from a supplied `folder`
    """
    docs = []
    for arch in os.listdir(folder):
        docs.append(os.path.join(folder, arch))
    return [(folder, docs)]
#
#
####################################
#
#      distutils.core.setup call
#
####################################
#
#
setup(
    windows=[
        {'script': PyName,
         'icon_resources': [(0, Icon)],
         'dest_base': AppName,
         'version': AppVersion,
         'company_name': "LP-CSIC/UAB",
         'copyright': Copyright,
         'name': AppName
         }
    ],

    options={
        'py2exe': {
            'packages':  ['matplotlib', 'pytz'],
            #'includes':     [],
            'excludes':  ['bsddb', 'curses', 'pywin.debugger',
                          'pywin.debugger.dbgcon', 'pywin.dialogs',
                          'tcl', 'Tkconstants', 'Tkinter', 'pygtk',
                          'pydoc', 'doctest', 'test', 'sqlite3',
                          'bsddb', 'curses', 'themail', '_fltkagg',
                          '_gtk', '_gtkcairo', '_agg2', '_gtkagg',
                          '_tkagg', '_cairo'
                           ],
            'ignores':     ['wxmsw26uh_vc.dll'],
            'dll_excludes': ['libgdk-win32-2.0-0.dll',
                             'libgobject-2.0-0.dll',
                             'tcl84.dll', 'tk84.dll',
                             'libgdk_pixbuf-2.0-0.dll',
                             'libgtk-win32-2.0-0.dll',
                             'libglib-2.0-0.dll',
                             'libcairo-2.dll',
                             'libpango-1.0-0.dll',
                             'libpangowin32-1.0-0.dll',
                             'libpangocairo-1.0-0.dll',
                             'libglade-2.0-0.dll',
                             'libgmodule-2.0-0.dll',
                             'libgthread-2.0-0.dll',
                             'QtGui4.dll', 'QtCore.dll',
                             'QtCore4.dll'
                              ],
            'compressed': 1,
            'optimize': 2,
            'bundle_files': 1
            }
            },
    zipfile=None,
    #Additional files I want in folder  'dist'.
    #The packer (inno) can go to the main dir to get them but then
    #they would not be in 'dist' and the program.exe there would not work.
    data_files = get_docs(SrcDocDir) + \
                 matplotlib.get_py2exe_datafiles() + [
                 ("", ["INSTALL.txt",    # InnoSetup Installer final page
                       "README.txt",     # README and help (English)
                       "LICENCE.TXT",    # Licence File (GPL v3)
                       "msvcp90.dll",
                       "C:\\Python27/Lib/site-packages/wx-2.8-msw-unicode/wx/gdiplus.dll",
                       "C:\\Python27/lib/site-packages/numpy/core/umath.pyd"
                       ]
                  ),
                  ("test", []),
                  ("images", [Sicon, Icon])
                  ]
    )
