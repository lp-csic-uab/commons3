#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
avisos (commons)
15 de agosto de 2010
OK
"""
#
import wx
#
#
__all__ = ['dame_ok', 'aviso'] 
#
#
def dame_ok(pregunta, titulo='Pregunta'):
    """
    Ventana de dialogo para cosas generales, OK y CANCEL
    """
    print "deprecated, use function commons.warnings.tell or is_ok"
    dialog = wx.MessageDialog(None, pregunta, titulo,
                                style=wx.OK|wx.CANCEL|wx.ICON_QUESTION)
    if dialog.ShowModal() == wx.ID_OK:
        dialog.Destroy()
        return True
    return False
#
#
def aviso(warning):
    print "deprecated, use function commons.warnings.tell"
    dlg = wx.MessageDialog(None, warning, style=wx.OK)
    if dlg.ShowModal() == wx.ID_OK:
        dlg.Destroy()
#
#
