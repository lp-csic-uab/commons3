#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
iconic (commons)
1 march 2012
"""
#
import wx
import base64
from io import BytesIO
from commons.images import DEFAULTICON
#
#
class Iconic():
    """Class with icon and bmp image.
    To be always a Mixin with a wx.Frame or equivalent
    """
    
    def __init__(self, icon=None, bmp=(None, None)):
        """Constructor

        icon -> the icon
        bmp -> (string_image, target_widget)

        """
        my_ico = icon if icon else DEFAULTICON
        string_image, widget = bmp
        
        if string_image:
            self.set_bmp(string_image, widget)
        self.set_icon(my_ico)
     #
    def decode_string(self, string):
        data = base64.decodestring(bytes(string, 'utf8'))
        flhndl = BytesIO(data)
        return wx.Image(flhndl, wx.BITMAP_TYPE_ANY)
    #
    def set_bmp(self, the_bmp, widget):
        """"""
        image = self.decode_string(the_bmp)
        my_bmp = wx.Bitmap(image)
        try:
            widget.SetBitmap(my_bmp)
        except AttributeError:
            widget.SetBitmapLabel(my_bmp)   # bitmapbutton
    #
    #noinspection PyUnresolvedReferences
    def set_icon(self, ico):
        """"""
        img = self.decode_string(ico)
        _icon = wx.Icon(wx.Bitmap(img))
        self.SetIcon(_icon)
    #
#
#
class MyIconTest(wx.Frame, Iconic):
    """"""
    def __init__(self, *args, **kwds):
        """"""
        my_icon = kwds.pop('icon', None)
        my_bmp = kwds.pop('bmp', None)
        kwds["style"] = wx.DEFAULT_FRAME_STYLE | wx.FULL_REPAINT_ON_RESIZE
        #
        wx.Frame.__init__(self, *args, **kwds)
        self.bmp = wx.StaticBitmap(self, -1, wx.NullBitmap)
        #
        Iconic.__init__(self, icon=my_icon, bmp=(my_bmp, self.bmp))
#
#
#
if __name__ == "__main__":
    #
    from images import DEFAULTICON
            
    app = wx.App(0)
    myframe = MyIconTest(None, icon=DEFAULTICON, bmp=DEFAULTICON)
    myframe.Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()