#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
commons_info (commons)
1 march 2012
"""
#
import wx
from wx.html import HtmlWindow
#
#
ABOUT_TXT = ["About"]
#
#
#noinspection PyArgumentList,PyUnusedLocal
class MyHtmlWindow(HtmlWindow):
    def __init__(self, *args, **kwargs):
        HtmlWindow.__init__(self, *args, **kwargs)

    def OnLinkClicked(self, linkinfo):
        url = linkinfo.GetHref()
        if url.startswith('http'):
            import webbrowser
            webbrowser.open(url)
        else:
            super(MyHtmlWindow, self).OnLinkClicked(linkinfo)
#
#
#noinspection PyArgumentList,PyUnusedLocal
class About(wx.MiniFrame):
    """Frame with the About text"""
    exists = False
    def __init__(self, source="doc/README.html", type='html', pos=(100, 100)):
        wx.MiniFrame.__init__(self, None, -1, "", pos,
                              style=wx.DEFAULT_FRAME_STYLE)
        #
        About.exists = True
        if type == 'text':
            minsize =  520, 300
            maxsize =  520, 700
            #
            try:
                texto = open(source).read()
            except IOError:
                texto = '\n'.join(ABOUT_TXT)
            #
            self.tc_readme = wx.TextCtrl(self, -1, texto,
                                         style=wx.TE_MULTILINE|wx.TE_READONLY)
        else:
            minsize = 500, 300
            maxsize = 800, 700
            #
            self.tc_readme = MyHtmlWindow(self, -1)
            self.tc_readme.LoadFile(source)

        self.SetTitle('Joaquin Abian Noviembre 2008')
        self.bt_close = wx.Button(self, -1, 'OK', size=(-1,-1))
        #
        self.Bind(wx.EVT_BUTTON, self.on_close, self.bt_close)
        self.Bind(wx.EVT_CLOSE, self.on_close)
        #
        sizerv = wx.BoxSizer(wx.VERTICAL)
        sizerv.Add(self.tc_readme, 1, wx.EXPAND)
        sizerv.Add(self.bt_close, 0, wx.EXPAND)
        #
        self.SetSizer(sizerv)
        self.Fit()
        self.SetMinSize(minsize)
        self.SetMaxSize(maxsize)
        self.SetSize(maxsize)

    def on_close(self, evt):
        About.exists = False
        self.Destroy()
#
#
#
if __name__ == '__main__':   
    app = wx.PySimpleApp()
    About(type='text').Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()
