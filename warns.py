#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
avisos (commons)
15 de agosto de 2010
OK
"""
#
import wx
#noinspection PyUnresolvedReferences
from wx._core import PyNoAppError
#
# 
__all__ = ['dame_ok', 'tell']
#
#
error_fatal = (KeyboardInterrupt, MemoryError)
#
#
def tell(avis, title="", level='normal'):
    """Popup warning. 'question' and 'fatal' levels show a "Cancel" button.

    :level: 'normal', 'question', 'fatal'

    Returns 0 when pressing "Cancel", otherwise returns 1.
    This 0 can be used to re-raise the error and stop the
    program on a fatal warning.

    """
    normal_style = wx.OK|wx.ICON_EXCLAMATION
    question_style = wx.OK|wx.CANCEL|wx.ICON_QUESTION
    fatal_style = wx.OK|wx.CANCEL|wx.ICON_WARNING

    style = {'normal':normal_style,
             'question':question_style,
             'fatal':fatal_style
            }

    try:
        dlg = wx.MessageDialog(None, avis, caption=title, style=style[level])
    except PyNoAppError:
        #noinspection PyUnusedLocal
        App = wx.App()
        dlg = wx.MessageDialog(None, avis, caption=title, style=style[level])

    result = dlg.ShowModal()

    if result == wx.ID_OK:
        dlg.Destroy()
        return True
    elif result == wx.ID_CANCEL:
        dlg.Destroy()

    return False
#
#
def is_ok(question, title='Question'):
    """Popup ask question and returns True or False"""
    tell(question, title=title, level='question')
#
#
if __name__ == '__main__':
    tell('hola', 'titulo', 'fatal')
