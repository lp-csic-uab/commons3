#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
A package with some common utilities
"""
#
#noinspection PyUnresolvedReferences
from . import warns, iconic, mass, mcalc
#
#
__version__ = '0.1.0'
#__revision__ = utils.get_git_revision()
__authors__ = 'Joaquin Abian'
__copyright__ = '2009-2019'
__license__ = 'GPLv3'
__contact__ = 'gatoygata2@gmail.com'


