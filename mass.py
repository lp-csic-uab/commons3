#!/usr/bin/env python
# -*- coding: utf-8 -*-

VERSION = 'version 0.3.3 July 2018'

#data calculated following ???
#from july 2010, except errors, I follow:
# http://www.sisweb.com/referenc/source/exactmaa.htm
# http://physics.nist.gov/cgi-bin/Compositions/stand_alone.pl?ele=&ascii=html&isotype=some
# C12 = 12.0000 monoisotopic
#
# for aa average: http://www.matrixscience.com/help/aa_help.html
# J=L=I  J is the accepted standard for L/I.
# X is now deprecated in my code to mean L/I. Now X means 'unknown'
# Here X mass = J mass but this is not kept for other parameters as AA probability
#
# mass_aa = {'Amino Acid Letter (Upper Case: not modified; lower case: modified)': (Average mass, Monoisotopic mass), ...}
mass_aa = {'G': (57.0513, 57.021464), 'A': (71.0779, 71.037114),
           'S': (87.0773, 87.032029), 'P': (97.1152, 97.052764),
           'V': (99.1311, 99.068414), 'T': (101.1039, 101.04768),
           'C': (103.1429, 103.00919), 'J': (113.1576, 113.08406),
           'L': (113.1576, 113.08406), 'I': (113.1576, 113.08406),
           'X': (113.1576, 113.08406), 'N': (114.1026, 114.04293),
           'D': (115.0874, 115.02694), 'Q': (128.1292, 128.05858),
           'K': (128.1723, 128.09496), 'E': (129.1140, 129.04259),
           'M': (131.1961, 131.04048), 'H': (137.1393, 137.05891),
           'm': (147.1955, 147.03595), 'F': (147.1739, 147.06841),
           'R': (156.1857, 156.10111), 'c': (160.20, 160.03065),
           'Y': (163.1733, 163.06333), 's': (167.0572, 166.99836),
           't': (181.0838, 181.01401), 'W': (186.2099, 186.07931),
           'y': (243.1532, 243.02963)
           }

# http://www.webelements.com/periodicity/atomic_weight/
#
# mass_atom = {'Atom Letter': (Average mass, Monoisotopic mass), ...}
mass_atom = {'H': (1.00794, 1.007825),   'C': (12.0107, 12.000),
             'N': (14.0067, 14.003074),  'O': (15.9994, 15.994915),
             'P': (30.973762, 30.973762),'S': (32.0655, 31.972071)
             }

# mass electron (0.00054857990907 u)
EMASS = 0.00054858
# mass proton (1.007276466879 u)
PROTON = 1.007276

#from Applied Biosystems iTRAQ Reagents. Chemistry Reference Guide
#Part Number 4351918 Rev. A 05/2004
#exact mass added->144.1059, 144.0996, 144.1021, 144.1021 -> mean 144.102425
#itraq reporters -> 114.1112, 115.1083, 116.1116, 117.1150
#
#from Thermo Scientific TMT Isobaric Mass Tagging Kits and Reagents
# ref 2073.0
#Modification masses of the TMT Label Reagents.
#Label      Mass      Mass     Ion     Ion
#           mono      avg      mono    avg
#TMT0-126 224.1525 224.2994 126.1283 126.2193
#TMT2-126 225.1558 225.2921 126.1283 126.2193
#TMT2-127 225.1558 225.2921 127.1316 127.2120
#TMT6-126 229.1629 229.2634 126.1283 126.2193
#TMT6-127 229.1629 229.2634 127.1316 127.2120
#TMT6-128 229.1629 229.2634 128.1350 128.2046
#TMT6-129 229.1629 229.2634 129.1383 129.1973
#TMT6-130 229.1629 229.2634 130.1417 130.1900
#TMT6-131 229.1629 229.2634 131.1387 131.1834
#
# mass_group = {'Group Code': (Average mass, Monoisotopic mass), ...}
mass_group = {'H2O': (18.0152, 18.010565), 'Pho': (79.9799, 79.966331),
              'Ace': (42.0369, 42.010565), 'NO' : (28.9982, 28.9902),
              'Oxg': (15.9994, 15.994915), 'Met': (14.0267, 14.01565),
              'NO2': (44.9976, 44.985078), 'null': (0., 0.),
              'Sul': (80.0637, 79.956815), 'NH2': (16.0226, 16.0187),
              'For': (28.0101, 27.994915), 'itrq': (144.1544, 144.102063),
              'tmt': (229.2634, 229.1629), 'H'  : (1.00794, 1.007825),
              'GG': (114.1026, 114.042927)
             }

# UniProtKB/TrEMBL PROTEIN DATABASE RELEASE 2012_03 STATISTICS
# Composition for the complete database
#
#Ala (A) 8.60   Gln (Q) 3.91   Leu (L) 9.88   Ser (S) 6.73
#Arg (R) 5.47   Glu (E) 6.17   Lys (K) 5.25   Thr (T) 5.60
#Asn (N) 4.09   Gly (G) 7.10   Met (M) 2.47   Trp (W) 1.31
#Asp (D) 5.31   His (H) 2.21   Phe (F) 4.01   Tyr (Y) 3.03
#Cys (C) 1.28   Ile (I) 5.95   Pro (P) 4.77   Val (V) 6.75
#
#Asx (B) 0.000  Glx (Z) 0      Xaa (X) 0.03
#
prob_aa =  {'G': 0.0710, 'A': 0.0860, 'S': 0.0673,
            'P': 0.0477, 'V': 0.0675, 'T': 0.0560,
            'C': 0.0128, 'J': 0.0792, 'L': 0.0988,    # J is mean of L,I
            'I': 0.0595, 'X': 0.0003, 'N': 0.0409,
            'D': 0.0531, 'Q': 0.0391, 'K': 0.0525,
            'E': 0.0617, 'M': 0.0247, 'H': 0.0221,
            'm': 0.0247, 'F': 0.0401, 'R': 0.0547,
            'c': 0.0128, 'Y': 0.0303, 's': 0.0673,
            't': 0.0560, 'W': 0.0131, 'y': 0.0303
            }
#
#frequency in vertebrates
#http://www.tiem.utk.edu/bioed//webmodules/aminoacid.htm
prob_aa_vert = {'G': 0.074, 'A': 0.074, 'S': 0.081,
                'P': 0.050, 'V': 0.068, 'T': 0.062,
                'C': 0.033, 'J': 0.057, 'L': 0.076,
                'I': 0.038, 'X': 0.001, 'N': 0.044,
                'D': 0.059, 'Q': 0.037, 'K': 0.072,
                'E': 0.058, 'M': 0.018, 'H': 0.029,
                'm': 0.018, 'F': 0.040, 'R': 0.042,
                'c': 0.033, 'Y': 0.033, 's': 0.081,
                't': 0.062, 'W': 0.013, 'y': 0.033
                }
#
#frequency in brachyspira
#calculated from uniprot fasta 2013
prob_aa_brac = {'G': 0.054051, 'A':0.058840, 'S':0.067089,
                'P': 0.026210, 'V':0.056320, 'T':0.048173,
                'C': 0.008848, 'J':0.094000, 'L':0.086249,
                'I': 0.103145, 'X':0.000003, 'N':0.076614,
                'D': 0.060855, 'Q':0.020520, 'K':0.088616,
                'E': 0.069878, 'M':0.025406, 'H':0.013112,
                'm': 0.025406, 'F':0.048155, 'R':0.031440,
                'c': 0.008848, 'Y':0.050242, 's':0.067089,
                't': 0.048173, 'W':0.006234, 'y':0.050242,
                'U': 0.000001
                }
#
# Possible aminoacid symbols
AA = ['G', 'A', 'S', 'P', 'V', 'T', 'C', 'L', 'I',
      'N', 'D', 'Q', 'K', 'E', 'M', 'H',
      'F', 'R',  'Y',  'W',
      # m= osxidated methionine
      # c= cysteine carbamidomethylated
      'm','c',
      # unknown
      'X',
      # J=leucine or isoleucine (I/L)
      # B=aspartic or asparagine (D/N),
      # Z=glutamic or glutamine (E/Q)
      'J', 'B', 'Z'
      # phospho amino acids
      's', 't', 'y'
      ]

def get_mass(item, mode='mono', water=True, test=False):
    """Determines peptide mass.

    mode = 'mono' / 'avg' (Mr monoisotopic / average)
    water = True / False (normal peptide / no terminal H OH)
    """
    if mode == 'avg':
        idx = 0
    elif mode == 'mono':
        idx = 1
    else:
        raise ValueError

    mass = mass_group['H2O'][idx] if water else 0
    #
    for char in item:
        try:
            mass += mass_aa[char][idx]
        except KeyError:
            if test:
                print('bizarre AA ->%s<-' % char)

    return mass
#
#
if __name__ == '__main__':
    #test program
    #
    from time import time
    peptides = ['FtGHyRKASDsFR', 'BFGHRKX', 'FGHRK',
                'GASPVTCXLINDQKEMHmFRcYstWy', 'ISDVCM', 'ISDVcM', 'ISDVCm']
    #
    print(VERSION)
    #
    time0 = time()
    #
    text = 'mass peptide %15s = %8.2f (avg), %12f (mono)'
    rep = 1000
    for peptide in peptides:
        for i in range(rep):
            average = get_mass(peptide, 'avg', False)
            #noinspection PyArgumentEqualDefault
            mono_iso = get_mass(peptide, 'mono', False)

        average = get_mass(peptide, 'avg', False, test=True)
        #noinspection PyArgumentEqualDefault
        mono_iso = get_mass(peptide, 'mono', False, test=True)
        print( text % (peptide, average, mono_iso) )
    #
    time1 = time()
    print( 'search time = %f for %i repetitions' % (time1 - time0, rep) )

#    C:\Python26\python.exe C:/Python26/programas/commons/mass.py
#    version 0.2 july 2010
#    mass peptide   FtGHyRKASDsFR =  1793.66 (avg),  1792.652457 (mono)
#    bizarre AA ->B<-
#    bizarre AA ->B<-
#    mass peptide         BFGHRKX =   738.89 (avg),   738.428914 (mono)
#    mass peptide           FGHRK =   625.73 (avg),   625.344854 (mono)
#    mass peptide GASPVTCXLINDQKEMHmFRcYstWy =  3389.68 (avg),  3387.306985 (mono)
#    mass peptide          ISDVCM =   648.81 (avg),   648.261113 (mono)
#    mass peptide          ISDVcM =   705.86 (avg),   705.282573 (mono)
#    mass peptide          ISDVCm =   664.81 (avg),   664.256583 (mono)
#    search time = 0.037000 for 1000 repetitions
