#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
mcal (commons)
5 may 2011
"""
#
import wx
from commons.mass import get_mass, mass_group
#
#
#
#noinspection PyUnusedLocal
class MiniCalc(wx.Panel):
    """Mini calculator of Peptide molecular masses.

    Function <on_bt_calc(self, evt)>
      - must be implemented on the parent frame
      - triggered by 'wx.EVT_BUTTON' from 'self.bt_calc'.
      - should call <self.calc([sequence])> after defining 'self.avg' and/or
        'self.water' if needed.

    """
    #noinspection PyArgumentList
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                                 size=(100, 93), style=wx.NO_BORDER):
        wx.Panel.__init__(self, parent, ID, pos, size, style)

        self.SetMinSize(size)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer3 = wx.BoxSizer(wx.VERTICAL)
        #
        self.pw = parent
        self.avg = 'mono'
        self.water = True
        self.memory = []
        self.result = 0
        self.sequence = None
        self.enter_manual = False
        self.ptm_key = 'null'
        #
        for i in range(20):
            self.memory.append('')
        #
        self.cbbx_choices = ['%3i  %4s' % (round(float(note[0])), key)
                             for key, note in mass_group.items()]
        #
        size1 = (74, 22)   # peptide mass
        size2 = (62, 22)   # modification
        size3 = (24, 22)   # clr
        size4 = (45, 22)   # calc
        size5 = (28, 22)   # +,-, [::]
        size6 = (28, 24)   # iso, ->,<-
        #
        self.tc = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.tc_sum = wx.TextCtrl(self, size=size1,
                                  style=wx.TE_READONLY | wx.TE_NOHIDESEL | wx.TE_CENTRE)
        #
        self.cb_num = wx.ComboBox(self, choices=self.cbbx_choices, size=size2,
                                  style=wx.TE_CENTRE)
        self.bt_clr = wx.Button(self, label='clr', size=size3)
        self.bt_calc = wx.Button(self, label='calc', size=size4)
        self.bt_next = wx.Button(self, label='->', size=size5)
        self.bt_prev = wx.Button(self, label='<-', size=size5)
        self.bt_mas = wx.Button(self, label='+', size=size5)
        self.bt_menos = wx.Button(self, label='-', size=size5)
        self.bt_iso = wx.Button(self, label='iso', size=size6)
        self.bt_clone = wx.Button(self, label='[::]', size=size6)
        #
        self.SetBackgroundColour("yellow")
        self.tc.SetBackgroundColour(wx.Colour(200, 255, 100))
        self.bt_calc.SetBackgroundColour(wx.Colour(255, 0, 0))
        self.cb_num.SetBackgroundColour(wx.Colour(255, 155, 220))
        self.tc_sum.SetBackgroundColour(wx.Colour(255, 255, 100))
        #
        self.tc_sum.SetToolTip("Calculated mass")
        self.bt_calc.SetToolTip("Press to calculate mass")
        self.bt_clr.SetToolTip('Clear entry')
        self.cb_num.SetToolTip('Group mass')
        self.bt_mas.SetToolTip('Add group mass')
        self.bt_menos.SetToolTip('Subtract group mass')
        self.bt_next.SetToolTip('Next sequence in memory')
        self.bt_prev.SetToolTip('Previous sequence in memory')
        self.bt_iso.SetToolTip('Switch isotopic/average mass')
        self.bt_clone.SetToolTip('Clone Calculator')
        #
        self.Bind(wx.EVT_BUTTON, self.on_bt_calc, self.bt_calc)
        self.Bind(wx.EVT_BUTTON, self.on_bt_clr, self.bt_clr)
        self.Bind(wx.EVT_BUTTON, self.on_bt_mas, self.bt_mas)
        self.Bind(wx.EVT_BUTTON, self.on_bt_menos, self.bt_menos)
        self.Bind(wx.EVT_BUTTON, self.on_bt_next, self.bt_next)
        self.Bind(wx.EVT_BUTTON, self.on_bt_prev, self.bt_prev)
        self.Bind(wx.EVT_BUTTON, self.on_bt_iso, self.bt_iso)
        self.Bind(wx.EVT_BUTTON, self.on_bt_clone, self.bt_clone)
        self.Bind(wx.EVT_COMBOBOX, self.on_select, self.cb_num)
        self.Bind(wx.EVT_TEXT, self.on_cbx_text, self.cb_num)
        #
        # This allows to destroy de separator ? I couldn't
        self.separator = wx.Size(2,2)
        #
        self.sizer2.Add(self.tc_sum, 1, wx.EXPAND)
        self.sizer2.Add(self.bt_calc, 0, wx.EXPAND)
        self.sizer2.Add(self.bt_clr, 0, wx.EXPAND)
        self.sizer2.Add((2, 2))
        self.sizer2.Add(self.cb_num, 0, wx.EXPAND)
        self.sizer2.Add(self.bt_mas, 0, wx.EXPAND)
        self.sizer2.Add(self.bt_menos, 0, wx.EXPAND)
        self.sizer2.Add(self.separator)
        self.sizer2.Add(self.bt_clone, 0, wx.EXPAND)
        #
        self.sizer3.Add(self.bt_iso, 0, wx.EXPAND)
        self.sizer3.Add((2, 2), 1)
        self.sizer3.Add(self.bt_prev, 0, wx.EXPAND)
        self.sizer3.Add(self.bt_next, 0, wx.EXPAND)
        self.sizer3.Add((2, 2), 1)
        #
        self.sizer1.Add(self.tc, 1, wx.EXPAND)
        self.sizer1.Add(self.sizer3, 0, wx.EXPAND)
        #
        self.sizer.Add(self.sizer1, 1, wx.EXPAND)
        self.sizer.Add(self.sizer2, 0, wx.EXPAND)
        #
        self.SetSizer(self.sizer)
        self.Fit()

    def on_bt_clr(self, evt):
        """"""
        self.clear()

    def on_bt_mas(self, evt):
        """"""
        self.opera('mas')
    #
    def on_bt_menos(self, evt):
        """"""
        self.opera('menos')
    #
    def clear(self):
        """"""
        self.result = 0
        self.tc_sum.Clear()
        self.tc.Clear()
    #
    def opera(self, mode):
        """"""
        if self.enter_manual:
            val = self.cb_num.GetValue()
        else:
            self.get_ptm_to_add()
            val = self.ptm_to_add
        #
        if not val:
            val = 0
        #
        try:
            to_add = float(val)
        except ValueError:
            to_add = 0
        #
        if mode == 'mas':
            self.result += to_add
        elif mode == 'menos':
            self.result -= to_add
        #
        self.write_result()
    #
    def on_bt_next(self, evt):
        """"""
        txt = self.memory[10]
        for i in range(18):
            self.memory[i] = self.memory[i + 1]
        self.memory[18] = ''
        self.tc.Clear()
        self.tc.WriteText(txt)
    #
    def on_bt_prev(self, evt):
        """"""
        txt = self.memory[8]
        for i in range(18):
            self.memory[18 - i] = self.memory[18 - i - 1]
        self.memory[0] = ''
        self.tc.Clear()
        self.tc.WriteText(txt)
    #
    def on_bt_calc(self, evt):
        """To be implemented. Sends event signal to the parent frame.

        Run calculation.
        """
        evt.Skip()
    #
    def write_selection(self):
        """"""
        while self.memory[9]:
            for i in range(18):
                self.memory[i] = self.memory[i + 1]
            self.memory[18] = ''
        #
        self.memory[9] = self.selection
        self.tc.SetValue(self.selection)
    #
    def calc(self, selection=None):
        """Calculates peptide mass.

        By default mass is calculated with water included (self.water = True)
        In practice, for amidated peptides for example, first subtract the
        water mass and then add 'H' and 'NH2'.
        """
        if selection:
            self.selection = selection
        else:
            self.selection = self.tc.GetValue()

        self.write_selection()
        self.result = get_mass(self.selection,
                                       mode=self.avg, water=self.water)
        self.write_result()
    #
    def write_result(self):
        """"""
        if self.result < 9999:
            format_ = '%.4f'
        elif self.result < 99999:
            format_ = '%.3f'
        elif self.result < 999999:
            format_ = '%.2f'
        else:
            format_ = '%.1f'
        #
        self.tc_sum.SetValue(format_ % self.result)
    #
    def on_cbx_text(self, evt):
        """"""
        self.enter_manual = True
    #
    def on_select(self, evt):
        """
        """
        value = self.cb_num.GetValue()
        self.ptm_key = value.split()[1]
        self.get_ptm_to_add()
        wx.CallAfter(self.set_txt)
    #
    def get_ptm_to_add(self):
        """"""
        avg_idx = 0 if self.avg == 'avg' else 1
        self.ptm_to_add = mass_group[self.ptm_key][avg_idx]
    #
    def set_txt(self):
        """"""
        self.cb_num.SetValue('%.1f' % self.ptm_to_add)
        wx.CallAfter(self.set_manual)
    #
    def set_manual(self):
        """"""
        self.enter_manual = False
    #
    def on_bt_clone(self, evt):
        """To be implemented. Sends event signal to the parent frame.

        Determines how calculator clone copies are created.
        """
        evt.Skip()
    #
    def on_bt_iso(self, evt):
        """"""
        if self.avg == 'mono':
            self.bt_iso.SetBackgroundColour(wx.Colour(255, 0, 0))
            self.bt_iso.SetLabel('avg')
            self.avg = 'avg'
        else:
            self.bt_iso.SetBackgroundColour(wx.Colour(200, 200, 200))
            self.bt_iso.SetLabel('iso')
            self.avg = 'mono'
#
#
class CalcTestFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds)
        #
        self.calc = MiniCalc(self, style=wx.RAISED_BORDER)
        self.SetSize((320, 150))
        self.SetTitle("MiniCalculator Test")
        self.Bind(wx.EVT_BUTTON, self.on_bt_calc, self.calc.bt_calc)

    #noinspection PyUnusedLocal
    def on_bt_calc(self, evt):
        """Calculates peptide mass.

        Works with residues not with complete molecular masses

        """

        self.calc.water = False
        self.calc.calc()
#
#
if __name__ == "__main__":

    MyApp = wx.App()
    mini_calculator = CalcTestFrame(None)
    mini_calculator.Show()
    #noinspection PyUnresolvedReferences
    MyApp.MainLoop()
